<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Javascript Map Documentation</title>
  </head>
  <body>
    <div id="flex-body">
      <header>
        <nav id="navbar">
          <header><h1>Javascript Map Documentation</h1></header>
        </nav>
      </header>
      <main id="main-doc">
        <section class="main-section">
          <header><h2>Introduction</h2></header>
          <p>The <code>Map</code> object holds key-value pairs and remembers the original insertion order of the keys. Any value (both objects and primitive values) may be used as either a key or a value.</p>
        </section>
        <section class="main-section">
          <header><h2>Description</h2></header>
          <p>A <code>Map</code> object iterates its elements in insertion order — 
          a <code>for...of</code> loop returns an array of
          <code>[key, value]</code> for each iteration.</p>

          <h3>Key equality</h3>
          <ul>
            <li>Key equality is based on the <code>sameValueZero</code> algorithm.</li>
            <li><code>NaN</code> is considered the same as NaN (even though NaN !== NaN) and all other values are considered equal according to the semantics of the === operator.</li>
            <li>In the current ECMAScript specification, -0 and +0 are considered equal, although this was not so in earlier drafts. See "Value equality for -0 and 0" in the Browser compatibility table for details.</li>
          </ul>

          <h3>Objects and maps compared</h3>
          <p><code>Object</code> is similar to <code>Map</code>—both let you set keys to values, retrieve those values, delete keys, and detect whether something is stored at a key. Because of this (and because there were no built-in alternatives), Objects have been used as Maps historically.</p>
          <p>However, there are important differences that make Map preferable in certain cases:</p>
          <ul>
            <li>The keys of an <code>Object</code> are <code>String</code> and 
              <code>Symbol</code>, whereas they can be any value for a Map (including functions, objects, or any primitive).</li>
            <li>The keys in <code>Map</code> are ordered, while keys added to 
              Object are not. Thus, when iterating over it, a <code>Map</code> object returns keys in order of insertion. </li>
            <li>You can get the size of a <code>Map</code> easily with the 
              <code>size</code> property, while the number of properties in an Object must be determined manually.</li>
            <li>A <code>Map</code> is an <code>iterable</code>, so it can be 
              directly iterated. Iterating over an <code>Object</code> requires obtaining its keys in some fashion and iterating over them.</li>
            <li>An <code>Object</code> has a prototype, so there are default keys in the map that could collide with your keys if you're not careful. </li>
            <li>A <code>Map</code> may perform better in scenarios involving frequent additions and removals of key-value pairs.</li>
          </ul>
        </section>
        <section class="main-section">
          <header><h2>Constructor</h2></header>
          <div class="class-property">
            <p><code>Map()</code></p>
            <p>Creates new <code>Map</code> objects.</p>
          </div>
        </section>
        <section class="main-section">
          <header><h2>Properties</h2></header>
          <div class="class-property">
            <p><code>Map.length</code></p>
            <p>The value of the <code>length</code> property is 0.</p>
            <p>To count how many entries are in a <code>Map</code> use
            <code>Map.prototype.size</code>.</p>
          </div>
          <div class="class-property">
            <p><code>get Map[@@species]</code></p>
            <p>The constructor function that is used to create derived objects.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype</code></p>
            <p>Represents the prototype for the <code>Map</code> constructor. 
            Allows the addition of properties to all <code>Map</code> objects.</p>
          </div>
        </section>
        <section class="main-section">
          <header><h2>Map instances</h2></header>
          <p>All <code>Map</code> instances inherit from 
          <code>Map.prototype</code>.</p>

          <h3>Properties</h3>
          <div class="class-property">
            <p><code>Map.prototype.constructor</code></p>
            <p>Returns the function that created an instance's prototype. This is the Map function by default.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.size</code></p>
            <p>Returns the number of key/value pairs in the <code>Map</code> object.</p>
          </div>
          <h3>Methods</h3>
          <div class="class-property">
            <p><code>Map.prototype.clear()</code></p>
            <p>Removes all key-value pairs from the Map object.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.delete(key)</code></p>
            <p>Returns true if an element in the Map object existed and has been 
            removed, or false if the element does not exist. 
            <code>Map.prototype.has(key)</code> will return false afterwards.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.entries()</code></p>
            <p>Returns a new <code>Iterator</code> object that contains an array 
            of <code>[key, value]</code> for each element in the Map object in insertion order.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.forEach(callbackFn[, thisArg])</code></p>
            <p>Calls <code>callbackFn</code> once for each key-value pair present 
            in the Map object, in insertion order. If a <code>thisArg</code> 
            parameter is provided to <code>forEach</code>, it will be used as the this value for each callback.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.get(key)</code></p>
            <p>Returns the value associated to the key, or <code>undefined</code> if there is none.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.has(key)</code></p>
            <p>Returns a boolean asserting whether a value has been associated to 
            the key in the <code>Map</code> object or not.</p>
          </div>
          <div class="class-property">
            <p><code>Map.prototype.keys()</code></p>
            <p>Returns a new <code>Iterator</code> object that contains the keys 
            for each element in the <code>Map</code> object in insertion order.</p>
          </div>
        </section>
        <section class="main-section">
          <header><h2>Examples</h2></header>
          <h3>Using the <code>Map</code> object</h3>
          <pre><code>
let myMap = new Map()

let keyString = 'a string'
let keyObj    = {}
let keyFunc   = function() {}

// setting the values
myMap.set(keyString, "value associated with 'a string'")
myMap.set(keyObj, 'value associated with keyObj')
myMap.set(keyFunc, 'value associated with keyFunc')

myMap.size              // 3

// getting the values
myMap.get(keyString)    // "value associated with 'a string'"
myMap.get(keyObj)       // "value associated with keyObj"
myMap.get(keyFunc)      // "value associated with keyFunc"

myMap.get('a string')    // "value associated with 'a string'"
                         // because keyString === 'a string'
myMap.get({})            // undefined, because keyObj !== {}
myMap.get(function() {}) // undefined, because keyFunc !== function () {}
          </code></pre>
          <h3>Using <code>NaN</code> as <code>Map</code> keys</h3>
          <p><code>NaN</code> can also be used as a key. Even though every NaN is not equal to itself (NaN !== NaN is true), the following example works because NaNs are indistinguishable from each other:</p>
          <pre><code>
let myMap = new Map()
myMap.set(NaN, 'not a number')

myMap.get(NaN) 
// "not a number"

let otherNaN = Number('foo')
myMap.get(otherNaN) 
// "not a number"
          </code></pre>
          <h3>Relation with Array objects</h3>
          <pre><code>
let kvArray = [['key1', 'value1'], ['key2', 'value2']]

// Use the regular Map constructor to transform a 2D key-value Array into a map
let myMap = new Map(kvArray)

myMap.get('key1') // returns "value1"

// Use Array.from() to transform a map into a 2D key-value Array
console.log(Array.from(myMap)) // Will show you exactly the same Array as kvArray

// A succinct way to do the same, using the spread syntax
console.log([...myMap])

// Or use the keys() or values() iterators, and convert them to an array
console.log(Array.from(myMap.keys())) // ["key1", "key2"]
          </code></pre>
        </section>
        <section class="main-section">
          <header><h2>Usage notes</h2></header>
          <p>Beware! Setting Object properties works for Map objects as well, and 
          can cause considerable confusion.</p>
          <p>Therefore, this still sort-of works....</p>
          <pre><code>
let wrongMap = new Map()
wrongMap['bla'] = 'blaa'
wrongMap['bla2'] = 'blaaa2'

console.log(wrongMap)  // Map { bla: 'blaa', bla2: 'blaaa2' }
          </code></pre>
          <p>...But, it does not behave as expected:</p>
          <pre><code>
wrongMap.has('bla')    // false
wrongMap.delete('bla') // false
console.log(wrongMap)  // Map { bla: 'blaa', bla2: 'blaaa2' }
          </code></pre>
          <p>And there's very little difference from the correct usage, anyway: </p>
          <pre><code>
let myMap = new Map()
myMap.set('bla','blaa')
myMap.set('bla2','blaa2')
console.log(myMap)  // Map { 'bla' => 'blaa', 'bla2' => 'blaa2' }

myMap.has('bla')    // true
myMap.delete('bla') // true
console.log(myMap)  // Map { 'bla2' => 'blaa2' }
          </code></pre>
        </section>
        <section class="main-section">
          <header><h2>Source</h2></header>
          <p>This page is extracted from the 
          <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map">MDN</a>
          and mainly used as an exercise.</p>
        </section>
      </main>
    </div>
    <script src="bundle.js"></script>
  </body>
</html>
