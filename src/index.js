import './css/style.scss'
import 'highlight.js/styles/github.css';
import hljs from 'highlight.js/lib/highlight';
import javascript from 'highlight.js/lib/languages/javascript';

hljs.registerLanguage('javascript', javascript);
hljs.initHighlightingOnLoad();

function createNavLink(section) {
  const aTag = document.createElement('a');
  aTag.className = 'nav-link';
  aTag.href = '#' + escapeSectionName(section);
  aTag.innerHTML = section;
  return aTag;
}

function escapeSectionName(name) {
  return name.split(' ').join('_');
}

(function () {
  const ulTag = document.createElement('ul');
  for (const section of document.querySelectorAll('.main-section')) {
    const h2Tag = section.firstElementChild.firstChild;
    section.id = escapeSectionName(h2Tag.innerHTML);
    const liTag = document.createElement('li');
    liTag.prepend(createNavLink(h2Tag.innerHTML));
    ulTag.append(liTag);
  }
  document.getElementById('navbar').append(ulTag);
})()
